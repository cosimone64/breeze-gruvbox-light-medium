
# Breeze-Gruvbox-Light-Medium

A GTK+ 2 and 3 light theme with inspired by Gruvbox colors. It's a
slightly modified version of https://www.gnome-look.org/p/1241289/

Currently, only the GTK+2 version has complete, working colors.
GTK+3 is still a work in progress.
